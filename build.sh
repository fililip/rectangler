#!/bin/bash

rm -rf out/data/*

mcs src/*.cs -o+ -sdk:4.7 -langversion:7.2 -platform:x64 -out:out/rectangler \
    -r:deps/MonoGame.Framework

cp deps/* out/
mkdir -p out/data
cp -r data/bin/DesktopGL/* out/data/