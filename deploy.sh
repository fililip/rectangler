#!/bin/bash

cd out; mkbundle --simple --static --library /usr/lib/libmono-native.so --library /usr/lib/libopenal.so --library /usr/lib/libSDL2.so -o rectangler.x86_64 --config config --machine-config /etc/mono/4.5/machine.config -z rectangler
rm rectangler
rm *.dll
rm config
chmod +x rectangler.x86_64