﻿#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0_level_9_1
#endif

Texture2D tex;
float middleValue;
float2 texelSeparation;
float intensity;
float colorIntensity;
float blendCoefficient;
float2 resolution;
static float3x3 convolutionKernel = float3x3(
	0.0, -1.0, 0.0,
	-1.0, 4.0, -1.0,
	0.0, -1.0, 0.0
);

sampler2D texSampler = sampler_state
{
	Texture = <tex>;
};

struct VertexShaderOutput
{
	float4 pos : SV_POSITION0;
	float4 color : COLOR0;
	float2 uv : TEXCOORD0;
};

float4 MainPS(VertexShaderOutput input) : COLOR
{
	float2 uv = input.uv;

	if (blendCoefficient > 0.0)
	{
		float4 color = float4(0.0, 0.0, 0.0, 0.0);
		float2 texel = 1.0 / resolution.xy;
		
		float3x3 kernel = convolutionKernel;
		kernel[1][1] = middleValue;

		for (int y = 0; y < 3; y++)
		for (int x = 0; x < 3; x++)
			color += tex2D(texSampler, uv + texel * float2(x - 1, y - 1) * texelSeparation) * kernel[x][y] * intensity;
		
		return lerp(tex2D(texSampler, uv), float4((color * input.color).rgb * colorIntensity, 1.0), blendCoefficient);
	} else return tex2D(texSampler, uv);
}

technique SpriteDrawing
{
	pass P0
	{
		PixelShader = compile PS_SHADERMODEL MainPS();
	}
};