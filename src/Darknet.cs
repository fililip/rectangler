﻿using System;
using System.Runtime.InteropServices;

namespace Darknet
{
    [StructLayout(LayoutKind.Sequential)]
    public struct BBox
    {
        public int x, y, w, h;
        public float probability;
        public int classID;
        public int trackID;
        public int frameCounter;
        public float x3D, y3D, z3D;
    }

    public class ObjectDetector : IDisposable
    {
        private const string libName = "darknet";
        private const int maxObjects = 1000;

        [DllImport(libName, EntryPoint = "init")]
        private static extern int InitializeYolo(string configurationFilename, string weightsFilename, int gpuID);

        [DllImport(libName, EntryPoint = "detect_image")]
        private static extern int DetectImage(string filename, ref BBoxContainer container);

        [DllImport(libName, EntryPoint = "detect_mat")]
        private static extern int DetectImage(IntPtr pArray, int nSize, ref BBoxContainer container);

        [DllImport(libName, EntryPoint = "dispose")]
        private static extern int DisposeYolo();

        [StructLayout(LayoutKind.Sequential)]
        private struct BBoxContainer
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = maxObjects)]
            public BBox[] candidates;
        }

        public ObjectDetector(string configurationFilename, string weightsFilename, int gpuID)
            => InitializeYolo(configurationFilename, weightsFilename, gpuID);

        public void Dispose() => DisposeYolo();

        public BBox[] Detect(string filename)
        {
            BBoxContainer container = new BBoxContainer();
            DetectImage(filename, ref container);

            return container.candidates;
        }

        public BBox[] Detect(byte[] imageData)
        {
            BBoxContainer container = new BBoxContainer();

            int size = Marshal.SizeOf(imageData[0]) * imageData.Length;
            IntPtr ptr = Marshal.AllocHGlobal(size);

            try
            {
                Marshal.Copy(imageData, 0, ptr, imageData.Length);
                int count = DetectImage(ptr, imageData.Length, ref container);
                if (count == -1)
                    throw new NotSupportedException($"{libName} has no OpenCV support");
            }
            catch { return null; }
            finally { Marshal.FreeHGlobal(ptr); }

            return container.candidates;
        }
    }
}
