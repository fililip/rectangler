﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Darknet3D
{
	public class Program
	{
		public static SpriteFont font;
		public static Texture2D empty;
		public static Vector2 resolution = new Vector2(1280, 720);

		[STAThread]
		static void Main(string[] args) => new DarknetDetector(args).Run();
	}
}