﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Darknet;

namespace Darknet3D
{
	class AnnotationRectangle
	{
		public double x, y, w, h, probability;
		public string name;

		public AnnotationRectangle() {}

		public AnnotationRectangle(double x, double y, double w, double h, string name, double probability)
		{
			this.x = x;
			this.y = y;
			this.w = w;
			this.h = h;
			this.name = name;
			this.probability = probability;
		}
	}

	class Image
	{
		public string path = "";
		public List<AnnotationRectangle> rectangles = new List<AnnotationRectangle>();
		public List<SelectionRect> rects = new List<SelectionRect>();
		public int selectedRect = 0;
		public List<int> rectHistory = new List<int>();
	}

	class LogEntry
	{
		public string text = "";
		public Vector2 position = Vector2.Zero;
		public Color color = new Color(1f, 1f, 1f, 0f);
		public Stopwatch timer = new Stopwatch();

		public LogEntry() => timer.Start();

		public void Draw(ref SpriteBatch spriteBatch)
			=> spriteBatch.DrawString(Program.font, text, position, color);
	}

	class Log
	{
		public int maxEntries = 8;
		List<LogEntry> logEntries = new List<LogEntry>();

		public void Add(string message)
		{
			LogEntry entry = new LogEntry();
			entry.text = message;
			
			float height = 0f;
			for (int i = 0; i < logEntries.Count; i++)
				height += Program.font.MeasureString(logEntries[i].text).Y;

			entry.position.X = -32f;
			entry.position.Y = Program.resolution.Y - 8f - height - Program.font.MeasureString(entry.text).Y;
			logEntries.Add(entry);
		}

		public void Update(GameTime gameTime)
		{
			float delta = (float)(gameTime.ElapsedGameTime.TotalSeconds * 20f);
			for (int i = 0; i < logEntries.Count; i++)
			{
				LogEntry entry = logEntries[i];

				float height = 0f;
				for (int j = 0; j < i; j++)
					height += Program.font.MeasureString(logEntries[j].text).Y;

				if (entry.timer.ElapsedMilliseconds > 150)
				{
					entry.position += delta * (new Vector2(12f, Program.resolution.Y - 12f - height - Program.font.MeasureString(entry.text).Y) - entry.position);
					entry.color.A += (byte)(delta * (150f - entry.color.A));
				}
			}

			if (logEntries.Count > maxEntries)
				logEntries.RemoveAt(0);
		}

		public void Resized()
		{
			for (int i = 0; i < logEntries.Count; i++)
			{
				float height = 0f;
				for (int j = 0; j < i; j++)
					height += Program.font.MeasureString(logEntries[j].text).Y;

				logEntries[i].position.Y = Program.resolution.Y - 8f - height - Program.font.MeasureString(logEntries[i].text).Y;
			}
		}

		public void Draw(ref SpriteBatch spriteBatch)
		{
			for (int i = 0; i < logEntries.Count; i++)
				logEntries[i].Draw(ref spriteBatch);
		}
	}

	class SelectionRect
	{
		public double x, y, w, h;
		public int uID = 0;
		public int classID = 0;
	}

	public class DarknetDetector : Game
	{
		ObjectDetector objectDetector;

		int uID = 0;

		List<string> allowedExtensions = new List<string>
		{
			".png",
			".jpg",
			".webp"
		};

		string[] args;
		GraphicsDeviceManager graphics;
		SpriteBatch spriteBatch;
		Effect shader;

		List<Image> images = new List<Image>();
		Texture2D currentImage;
		bool heightGreater = false;
		float aspectWidth = 0f, width = 0f,
			  aspectHeight = 0f, height = 0f;
		float zoomScale = 1f;
		Vector2 zoomOffset = Vector2.Zero;
		int currentImageIndex = 0;
		int currentClassIndex = 0;
		bool currentImageReady = false;

		Log log = new Log();

		bool objectDetectMode = false;

		bool prevPrev = false;
		bool prevNext = false;

		bool prevUp = false;
		bool prevDown = false;

		bool prevC = false;
		bool prevDel = false;
		bool prevX = false;

		bool prevS = false;
		bool prevZ = false;
		bool prevR = false;

		bool prevLMB = false;
		bool LMB = false;
		int LMBState = 0;

		bool prevRMB = false;
		bool RMB = false;
		int RMBState = 0;

		int prevScroll = 0;
		bool prevMMB = false;
		bool MMB = false;

		Vector2 dragPos = Vector2.Zero;
		Vector2 dragOffset = Vector2.Zero;
		Vector2 prevPos = Vector2.Zero, nextPos = Vector2.Zero;

		bool firstUpdate = true;

		Vector2 grabPos = Vector2.Zero;
		Vector2 rectPos = Vector2.Zero, rectSize = Vector2.Zero;

		public DarknetDetector(string[] args)
		{
			graphics = new GraphicsDeviceManager(this);
			Content.RootDirectory = "data";

			if (args.Length == 0)
			{
				if (!Directory.Exists("images"))
				{
					Console.WriteLine("Please provide a path");
					Environment.Exit(1);
				} else args = new string[] { "images" };
			} else
			{
				if (args.Length >= 2)
				{
					if (bool.Parse(args[1])) objectDetectMode = true;
				}
			}

			this.args = args;
		}

		public static void Assert(bool condition, string message = "")
		{
			if (!condition)
			{
				Console.WriteLine(message);
				Environment.Exit(1);
			}
		}

		private void Resized()
		{
			Program.resolution = new Vector2(Window.ClientBounds.Width, Window.ClientBounds.Height);
			shader.Parameters["resolution"].SetValue(Program.resolution);
			log.Resized();
		}

		protected override void Initialize()
		{
			IsMouseVisible = true;
			IsFixedTimeStep = false;
			Window.AllowUserResizing = true;

			Window.ClientSizeChanged += (s, ea) => Resized();

			graphics.IsFullScreen = false;
			graphics.PreferredBackBufferWidth = (int)Program.resolution.X;
			graphics.PreferredBackBufferHeight = (int)Program.resolution.Y;
			graphics.SynchronizeWithVerticalRetrace = true;
			graphics.ApplyChanges();
			
			base.Initialize();
		}

		private void LoadObjectDetector()
		{
			if (objectDetectMode)
			{
				log.Add("Loading object detector...");
				new Task(() =>
				{
					objectDetector = new ObjectDetector("yolov4.cfg", "yolov4.weights", 0);
					log.Add("Loaded object detector");
					LoadImage(0);
				}).Start();
			} else LoadImage(0);
		}

		private void LoadImage(int index)
		{
			currentImageReady = false;
			new Task(() =>
			{
				currentImage = Texture2D.FromFile(GraphicsDevice, images[index].path);

				if (objectDetectMode)
				{
					BBox[] bboxes = objectDetector.Detect(images[index].path);

					images[index].rectangles.Clear();

					foreach (BBox bbox in bboxes)
					{
						if (bbox.probability >= 0.1)
						{
							images[index].rectangles.Add
							(
								new AnnotationRectangle
								(
									bbox.x, bbox.y,
									bbox.w, bbox.h,
									"class " + bbox.classID,
									bbox.probability
								)
							);
						}
					}
				}

				ResetZoom();

				currentImageIndex = index;
				currentImageReady =  true;
			}).Start();
		}

		private void PreviousImage()
		{
			currentImageIndex--;
			if (currentImageIndex < 0)
				currentImageIndex = images.Count - 1;
			LoadImage(currentImageIndex);
		}

		private void NextImage()
		{
			currentImageIndex++;
			if (currentImageIndex > images.Count - 1)
				currentImageIndex = 0;
			LoadImage(currentImageIndex);
		}

		private void LoadAnnotations()
		{
			bool found = false;
			foreach (Image image in images)
			{
				try
				{
					string[] annotations = File.ReadAllLines(Path.GetDirectoryName(image.path) + "/" + Path.GetFileNameWithoutExtension(image.path) + ".txt");
					foreach (string annotation in annotations)
					{
						string[] split = annotation.Split(' ');
						SelectionRect rect = new SelectionRect();
						rect.classID = int.Parse(split[0]);
						rect.x = double.Parse(split[1]);
						rect.y = double.Parse(split[2]);
						rect.w = double.Parse(split[3]);
						rect.h = double.Parse(split[4]);
						image.rects.Add(rect);
						found = true;
					}
				} catch { }
			}

			if (found)
				log.Add("Loaded saved annotations");
		}

		private void SaveAnnotations()
		{
			foreach (Image image in images)
			{
				if (image.rects.Count > 0)
				{
					List<string> annotations = new List<string>();
					foreach (SelectionRect rect in image.rects)
						annotations.Add(rect.classID + " " + rect.x.ToString("0.000000") + " " + rect.y.ToString("0.000000") + " " + rect.w.ToString("0.000000") + " " + rect.h.ToString("0.000000"));
					File.WriteAllLines(Path.GetDirectoryName(image.path) + "/" + Path.GetFileNameWithoutExtension(image.path) + ".txt", annotations.ToArray());
				} else File.WriteAllText(Path.GetDirectoryName(image.path) + "/" + Path.GetFileNameWithoutExtension(image.path) + ".txt", "");
			}

			log.Add("Saved annotations");
		}

		protected override void LoadContent()
		{
			spriteBatch = new SpriteBatch(GraphicsDevice);

			Program.empty = new Texture2D(GraphicsDevice, 1, 1);
			Program.empty.SetData(new Color[] { Color.White });

			Program.font = Content.Load<SpriteFont>("fonts/ibmplexsans");
			shader = Content.Load<Effect>("shaders/convolution");

			shader.Parameters["intensity"].SetValue(1f);
			shader.Parameters["middleValue"].SetValue(4f);
			shader.Parameters["texelSeparation"].SetValue(Vector2.One);
			shader.Parameters["colorIntensity"].SetValue(1f);
			shader.Parameters["blendCoefficient"].SetValue(0f);

			Assert(Directory.Exists(args[0]), "Path is incorrect");

			List<string> paths = new List<string>();

			foreach (string s in Directory.EnumerateFiles(args[0]))
			if (allowedExtensions.IndexOf(Path.GetExtension(s)) != -1)
				paths.Add(Path.GetFullPath(s));

			paths.Sort();

			foreach (string s in paths)
			{
				Image image = new Image();
				image.path = s;
				images.Add(image);
			}

			Assert(images.Count > 0, "No images found");
			log.Add("Found " + images.Count + " images");

			LoadAnnotations();
			LoadObjectDetector();
		}

		private Vector2 GetNormalized(Vector2 point)
		{
			Vector2 ndc = point - new Vector2(width, height);
			ndc /= new Vector2(aspectWidth, aspectHeight);
			return ndc;
		}

		private Vector2 GetNormalizedZoom(Vector2 point)
		{
			Vector2 ndc = point - new Vector2(width, height) - zoomOffset;
			ndc /= new Vector2(aspectWidth, aspectHeight);
			ndc /= zoomScale;
			return ndc;
		}

		private Vector2 GetNormalizedSizeZoom(Vector2 size)
		{
			size /= new Vector2(aspectWidth, aspectHeight) * zoomScale;
			return size;
		}

		private Vector2 GetScreenCoordsFromRect(Vector2 coords)
		{
			Vector2 offset = new Vector2(Program.resolution.X - Program.resolution.X / 2f - aspectWidth / 2f, Program.resolution.Y - Program.resolution.Y / 2f - aspectHeight / 2f);
			coords = new Vector2((float)(coords.X * aspectWidth), (float)(coords.Y * aspectHeight)) + offset;

			return coords;
		}

		private Vector2 GetZoom()
		{
			return -(prevPos - nextPos) * new Vector2(aspectWidth * zoomScale, aspectHeight * zoomScale);
		}

		private void ResetZoom()
		{
			zoomScale = 1f;
			prevPos = nextPos = dragOffset = zoomOffset = Vector2.Zero;	
		}

		private void DrawOutline(Vector2 position, Vector2 size, int thickness, Color color)
		{
			float x = position.X;
			float y = position.Y;
			float w = size.X;
			float h = size.Y;

			spriteBatch.Draw(Program.empty, new Rectangle((int)x, (int)y, (int)w + thickness, thickness), null, color);
			spriteBatch.Draw(Program.empty, new Rectangle((int)(x + w), (int)y, thickness, (int)h + thickness), null, color);
			spriteBatch.Draw(Program.empty, new Rectangle((int)x, (int)(y + h), (int)w + thickness, thickness), null, color);
			spriteBatch.Draw(Program.empty, new Rectangle((int)x, (int)y, thickness, (int)h + thickness), null, color);
		}

		private void DrawText(Vector2 position, Vector2 offset, string text, int padding, Color color)
		{
			Vector2 size = Program.font.MeasureString(text);

			spriteBatch.Draw(Program.empty, new Rectangle((int)(position.X + offset.X), (int)(position.Y + offset.Y - size.Y - padding), (int)(size.X + padding * 2), (int)(size.Y + padding)), null, color);
			spriteBatch.DrawString(Program.font, text, new Vector2((int)(position.X + padding + offset.X), (int)(position.Y - size.Y - padding + offset.Y)), Color.Black);
		}

		private float Clamp(float value, float min, float max)
		{
			return value < min ? min : value > max ? max : value;
		}

		protected override void Update(GameTime gameTime)
		{
			if (firstUpdate)
			{
				Resized();
				firstUpdate = false;
			}
			
			if (Keyboard.GetState().IsKeyDown(Keys.Escape)) Exit();

			if (currentImageReady)
			{
				heightGreater = Program.resolution.X / (double)Program.resolution.Y > currentImage.Width / (double)currentImage.Height;

				bool prev = Keyboard.GetState().IsKeyDown(Keys.Left);
				if (prev != prevPrev)
				{
					if (prev) PreviousImage();
					prevPrev = prev;
				}

				bool next = Keyboard.GetState().IsKeyDown(Keys.Right);
				if (next != prevNext)
				{
					if (next) NextImage();
					prevNext = next;
				}

				int scroll = Mouse.GetState().ScrollWheelValue;
				if (scroll != prevScroll)
				{
					if (!MMB)
					{
						prevPos = GetNormalizedZoom(Mouse.GetState().Position.ToVector2());

						float delta = scroll - prevScroll;
						zoomScale += delta > 0 ? 0.25f : -0.25f;
						zoomScale = Clamp(zoomScale, 0.25f, 4f);

						nextPos = GetNormalized(Mouse.GetState().Position.ToVector2()) / zoomScale;

						dragOffset = Vector2.Zero;
					}
					prevScroll = scroll;
				}

				MMB = Mouse.GetState().MiddleButton == ButtonState.Pressed;
				if (MMB != prevMMB)
				{
					if (MMB)
						dragPos = Mouse.GetState().Position.ToVector2() - dragOffset;
					prevMMB = MMB;
				}

				if (MMB)
					dragOffset = Mouse.GetState().Position.ToVector2() - dragPos;

				zoomOffset = GetZoom() + dragOffset;

				bool R = Keyboard.GetState().IsKeyDown(Keys.R);
				if (R != prevR)
				{
					if (R) ResetZoom();
					prevR = R;
				}

				if (!objectDetectMode)
				{
					bool up = Keyboard.GetState().IsKeyDown(Keys.Up);
					if (up != prevUp)
					{
						if (up) currentClassIndex = Math.Min(currentClassIndex + 1, int.MaxValue);
						prevUp = up;
					}

					bool down = Keyboard.GetState().IsKeyDown(Keys.Down);
					if (down != prevDown)
					{
						if (down) currentClassIndex = Math.Min(currentClassIndex - 1, 0);
						prevDown = down;
					}

					LMB = Mouse.GetState().LeftButton == ButtonState.Pressed;
					if (LMB != prevLMB)
					{
						if (LMB)
						{
							grabPos = Mouse.GetState().Position.ToVector2();
							LMBState = 1;
						} else
						{
							if (LMBState == 1)
							{
								Vector2 pos = GetNormalizedZoom(rectPos);
								Vector2 size = GetNormalizedSizeZoom(rectSize);

								SelectionRect rect = new SelectionRect();
								rect.w = size.X;
								rect.h = size.Y;
								rect.x = pos.X + size.X / 2.0;
								rect.y = pos.Y + size.Y / 2.0;
								rect.uID = uID++;
								rect.classID = currentClassIndex;

								if (rectSize.X > 2 && rectSize.Y > 2)
								{
									images[currentImageIndex].rectHistory.Add(rect.uID);
									images[currentImageIndex].rects.Add(rect);

									images[currentImageIndex].rects.Sort((p, q) =>
									{
										return ((p.x + p.w + p.y + p.h) / 2.0) > ((q.x + q.w + q.y + q.h) / 2.0) ? -1 : 1;
									});
								}
							}
							LMBState = 0;
						}
						prevLMB = LMB;
					}

					if (LMB)
					{
						Vector2 pos, size;
						pos = grabPos;
						size = Mouse.GetState().Position.ToVector2() - pos;

						if (size.X < 0)
						{
							pos.X = Mouse.GetState().Position.X;
							size.X = grabPos.X - pos.X;
						}

						if (size.Y < 0)
						{
							pos.Y = Mouse.GetState().Position.Y;
							size.Y = grabPos.Y - pos.Y;
						}

						rectPos = pos;
						rectSize = size;
					}

					RMB = Mouse.GetState().RightButton == ButtonState.Pressed;
					if (RMB != prevRMB)
					{
						if (RMB) RMBState = 1;
						else
						{
							bool found = false;
							if (RMBState == 1)
							for (int i = 0; i < images[currentImageIndex].rects.Count; i++)
							{
								SelectionRect rect = images[currentImageIndex].rects[i];
								Vector2 mousePos = GetNormalizedZoom(Mouse.GetState().Position.ToVector2());
								Vector2 leftTop = new Vector2((float)(rect.x - rect.w / 2.0), (float)(rect.y - rect.h / 2.0));
								Vector2 bottomRight = new Vector2((float)(rect.x + rect.w / 2.0), (float)(rect.y + rect.h / 2.0));
								if (mousePos.X >= leftTop.X && mousePos.Y >= leftTop.Y && mousePos.X <= bottomRight.X && mousePos.Y <= bottomRight.Y)
								{
									images[currentImageIndex].selectedRect = i;
									found = true;
								}
							}
							if (!found) images[currentImageIndex].selectedRect = -1;
							RMBState = 0;
						}
						prevRMB = RMB;
					}

					bool C = Keyboard.GetState().IsKeyDown(Keys.C);
					if (C != prevC)
					{
						if (C) images[currentImageIndex].rects.Clear();
						prevC = C;
					}

					bool del = Keyboard.GetState().IsKeyDown(Keys.Delete);
					if (del != prevDel)
					{
						if (del && images[currentImageIndex].rects.Count > 0 && images[currentImageIndex].selectedRect != -1)
						{
							images[currentImageIndex].rects.RemoveAt(images[currentImageIndex].selectedRect);
							images[currentImageIndex].selectedRect = -1;
						}
						prevDel = del;
					}

					bool ctrl = Keyboard.GetState().IsKeyDown(Keys.LeftControl);

					bool S = Keyboard.GetState().IsKeyDown(Keys.S);
					if (S != prevS)
					{
						if (S && ctrl) SaveAnnotations();
						prevS = S;
					}

					bool Z = Keyboard.GetState().IsKeyDown(Keys.Z);
					if (Z != prevZ)
					{
						if (Z && ctrl && images[currentImageIndex].rects.Count > 0 && images[currentImageIndex].rectHistory.Count > 0)
						{
							int index = -1;
							for (int i = 0; i < images[currentImageIndex].rects.Count; i++)
							{
								if (images[currentImageIndex].rects[i].uID == images[currentImageIndex].rectHistory[images[currentImageIndex].rectHistory.Count - 1])
								{
									index = i;
									break;
								}
							}

							if (index != -1)
							{
								images[currentImageIndex].rects.RemoveAt(index);
								images[currentImageIndex].rectHistory.RemoveAt(images[currentImageIndex].rectHistory.Count - 1);
							}
						}
						prevZ = Z;
					}
				}

				bool X = Keyboard.GetState().IsKeyDown(Keys.X);
				if (X != prevX)
				{
					if (X)
					{
						float coeff = shader.Parameters["blendCoefficient"].GetValueSingle();
						coeff = coeff > 0.5f ? 0f : 1f;
						shader.Parameters["blendCoefficient"].SetValue(coeff);
					}
					prevX = X;
				}
			}

			log.Update(gameTime);

			base.Update(gameTime);
		}

		protected override void Draw(GameTime gameTime)
		{
			GraphicsDevice.Clear(Color.Black);

			spriteBatch.Begin(blendState: BlendState.NonPremultiplied);

			if (currentImageReady)
			{
				aspectWidth = heightGreater ? Program.resolution.Y * (currentImage.Width / (float)currentImage.Height) : Program.resolution.X;
				width = heightGreater ? Program.resolution.X / 2f - aspectWidth / 2f : 0;

				aspectHeight = !heightGreater ? Program.resolution.X * (currentImage.Height / (float)currentImage.Width) : Program.resolution.Y;
				height = !heightGreater ? Program.resolution.Y / 2f - aspectHeight / 2f : 0;

				double scale = (double)Program.resolution.Y / currentImage.Height;
				scale *= zoomScale;

				shader.Parameters["texelSeparation"].SetValue(new Vector2
				(
					(float)Math.Sin(gameTime.TotalGameTime.TotalMilliseconds / 200f) * 0.3f + 0.9f,
					(float)Math.Cos(gameTime.TotalGameTime.TotalMilliseconds / 100f) * 0.3f + 0.9f
				));

				if (shader.Parameters["blendCoefficient"].GetValueSingle() > 0f)
				{
					spriteBatch.End();
					spriteBatch.Begin(blendState: BlendState.NonPremultiplied, effect: shader);
				}
				spriteBatch.Draw(currentImage, new Rectangle((int)(Program.resolution.X / 2f - aspectWidth / 2f + zoomOffset.X), (int)(Program.resolution.Y / 2f - aspectHeight / 2f + zoomOffset.Y),
															 (int)(aspectWidth * zoomScale), (int)(aspectHeight * zoomScale)), null, Color.White);
				if (shader.Parameters["blendCoefficient"].GetValueSingle() > 0f)
				{
					spriteBatch.End();
					spriteBatch.Begin(blendState: BlendState.NonPremultiplied);
				}

				int rectThickness = 2;
				int padding = 4;

				foreach (AnnotationRectangle rect in images[currentImageIndex].rectangles)
				{
					double x = Program.resolution.X / 2.0 - aspectWidth / 2.0 + rect.x * scale;
					double y = Program.resolution.Y / 2.0 - aspectHeight / 2.0 + rect.y * scale;
					double w = rect.w * scale;
					double h = rect.h * scale;

					string text = rect.name + ": " + (rect.probability * 100.0).ToString("00.00") + "%";
					Vector2 offset = new Vector2(0, rectThickness) + zoomOffset;
					Vector2 size = Program.font.MeasureString(text);

					DrawOutline(new Vector2((float)x + zoomOffset.X, (float)y + zoomOffset.Y), new Vector2((float)w, (float)h), rectThickness, Color.Red);

					spriteBatch.Draw(Program.empty, new Rectangle((int)(x + offset.X), (int)(y + offset.Y - size.Y - padding), (int)(size.X + padding * 2), (int)(size.Y + padding)), null, Color.Red);
					DrawText(new Vector2((float)x, (float)y), offset, text, padding, Color.Red);
				}

				for (int i = 0; i < images[currentImageIndex].rects.Count; i++)
				{
					SelectionRect rect = images[currentImageIndex].rects[i];
					Vector2 offset = new Vector2(Program.resolution.X - Program.resolution.X / 2f - aspectWidth / 2f,
												 Program.resolution.Y - Program.resolution.Y / 2f - aspectHeight / 2f) + zoomOffset; 

					Vector2 pos = new Vector2((float)((rect.x - rect.w / 2.0) * aspectWidth), (float)((rect.y - rect.h / 2.0) * aspectHeight)) * zoomScale + offset;
					Vector2 size = new Vector2((float)(rect.w * aspectWidth), (float)(rect.h * aspectHeight)) * zoomScale;

					DrawOutline(pos, size, 2, i == images[currentImageIndex].selectedRect ? Color.Yellow : Color.Red);
					DrawText(pos, new Vector2(0, 2), "class " + rect.classID, 4, i == images[currentImageIndex].selectedRect ? Color.Yellow : Color.Red);
				}
			}

			if (LMB) DrawOutline(rectPos, rectSize, 2, Color.Red);

			string infoText = "Navigation: < >\n";
			infoText += "Image " + (currentImageIndex + 1) + " of " + images.Count + "\n";
			infoText += "Current class ID: " + currentClassIndex + "\n";
			infoText += "Convolution: " + (shader.Parameters["blendCoefficient"].GetValueSingle() > 0.5f ? "on" : "off");

			Vector2 infoTextSize = Program.font.MeasureString(infoText);

			spriteBatch.Draw(Program.empty, new Rectangle(8, 8, (int)infoTextSize.X + 12, (int)infoTextSize.Y + 6), new Color(0.1f, 0.1f, 0.1f, 0.6f));
			spriteBatch.DrawString(Program.font, infoText, new Vector2(12, 10), Color.White);

			log.Draw(ref spriteBatch);

			spriteBatch.End();

			base.Draw(gameTime);
		}

		protected override void OnExiting(object sender, EventArgs args)
		{
			if (objectDetectMode) objectDetector.Dispose();
			Process.GetCurrentProcess().Kill();
		}
	}
}